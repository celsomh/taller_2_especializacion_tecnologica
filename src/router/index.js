import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Venta from '../components/Venta'
import CarroCompras from '../components/CarroCompras'
import HistorialVentas from '../components/HistorialVentas'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/venta',
    name: 'Venta',
    component: Venta
  },
  {
    path: '/carro-compras',
    name: 'CarroCompras',
    component: CarroCompras
  },
  {
    path: '/historial-ventas',
    name: 'HistorialVentas',
    component: HistorialVentas
  }
]

const router = new VueRouter({
  routes
})

export default router

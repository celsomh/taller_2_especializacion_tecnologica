import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    almacen: [
      { nombre: "Cuartel", descripcion: "Edificio militar usado para entrenar y mejorar unidades de infantería", precio: "175", stock: 10 },
      { nombre: "Galería de tiro con Arco", descripcion: "Edificio militar usado para entrenar y mejorar unidades de arquería", precio: "175", stock: 20 },
      { nombre: "Herrería", descripcion: "Edificio usado para desarrollar tecnologías para mejorar el ataque y la armadura de las unidades", precio: "150", stock: 10 }
    ],
    carroDeCompras: [],
    historialDeCompras: []

  },
  getters: {
    totalVentas(state) {
      let total=0
      state.historialDeCompras.forEach((compra) => {
        compra.forEach((productos) => {
          total+= (productos.producto.precio * productos.cantidad)
        })
      })
      return total
    }
  },
  mutations: {
    agregarProductoAlAlmacen(state, productoNuevo) {
      state.almacen.push(productoNuevo)
    },
    eliminarProductoDelAlmacen(state, index) {
      state.almacen.splice(index, 1)
    },
    agregarProductoAlCarro(state, indexCantidad) {
      state.carroDeCompras.push(indexCantidad)
      state.almacen[indexCantidad.index].stock -= indexCantidad.cantidad;
    },
    actualizarProducto(state, actualizacionDeProducto) {
      state.almacen[actualizacionDeProducto.index] = {
        nombre: actualizacionDeProducto.nombre,
        descripcion: actualizacionDeProducto.descripcion,
        precio: actualizacionDeProducto.precio,
        stock: actualizacionDeProducto.stock
      }
    }
    ,
    vaciarCarroDeCompras(state) {
      state.carroDeCompras = []
    },
    removerProductoDelCarroDeCompras(state, index) {
      state.carroDeCompras.splice(index, 1)
    },
    realizarCompra(state) {
      //Agregar los productos comprados dentro de un objeto "compra" para luego guardar tal objeto en el historial
      let compra = []
      state.carroDeCompras.forEach((productoEnCarro) => {
        compra.push({
          producto: {
            nombre: state.almacen[productoEnCarro.index].nombre,
            descripcion: state.almacen[productoEnCarro.index].descripcion,
            precio: state.almacen[productoEnCarro.index].precio,
          },
          cantidad: productoEnCarro.cantidad
        })

      })
      state.historialDeCompras.push(compra)
    }
  }
})
